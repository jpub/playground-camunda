package com.play

import org.apache.camel.AggregationStrategy
import org.apache.camel.Exchange
import org.apache.camel.Processor
import org.apache.camel.builder.RouteBuilder
import org.apache.camel.model.dataformat.JsonLibrary
import org.apache.camel.model.rest.RestBindingMode
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class MyOdmClaimSubmissionRoute: RouteBuilder() {
    override fun configure() {
        // without this binding mode got to convert dto to string manually
//        restConfiguration().host("localhost").port(7700).bindingMode(RestBindingMode.json)
        from("direct:odm-claim-submission") // OdmSubmissionDTO
            .marshal().json(JsonLibrary.Jackson, OdmSubmissionDTO::class.java)
            .log("odm-claim-submission before: \${body}")
            .to("rest:post:/api/flowClaimCalculation/?host=localhost:7700")
            .log("odm-claim-submission after: \${body}")
            .convertBodyTo(String::class.java) // weird.. without this it will be CachedOutputStream
    }
}

@Component
class MyFsrChildRequestRoute: RouteBuilder() {
    override fun configure() {
        from("direct:fsr-child-request")
            .log("fsr-child-request before: \${body}")
            .setHeader("parentId", simple("\${body}"))
            .to("rest:get:/api/fsr/retrieveChild/{parentId}/?host=localhost:7700")
            .log("fsr-child-request after: \${body}")
            .convertBodyTo(String::class.java) // weird.. without this it will be CachedOutputStream
    }
}
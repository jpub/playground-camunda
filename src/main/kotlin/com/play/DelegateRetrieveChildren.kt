package com.play

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.camunda.bpm.engine.delegate.DelegateExecution
import org.camunda.bpm.engine.delegate.JavaDelegate
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component

@Component
class DelegateRetrieveChildren : JavaDelegate {
    private val logger = LoggerFactory.getLogger(this.javaClass)

    override fun execute(execution: DelegateExecution) {
        logger.info("exeActivityName: ${execution.currentActivityName}")
        val mapper = jacksonObjectMapper()
        val odmMaxClaimDTOstring = execution.getVariable("DelegateClaimReceived") as String
        val odmMaxClaimDTO = mapper.readValue(odmMaxClaimDTOstring, OdmMaxClaimDTO::class.java)
        execution.setVariable("DelegateRetrieveChildren", odmMaxClaimDTO.myMaxClaimDetails.parentId)
    }
}

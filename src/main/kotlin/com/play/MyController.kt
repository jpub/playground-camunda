package com.play

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.camunda.bpm.engine.RuntimeService
import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.annotation.Resource

@RestController
@RequestMapping("/api")
class MyController {
    @Resource
    private lateinit var runtimeService: RuntimeService

    private val logger = LoggerFactory.getLogger(this.javaClass)

    @PostMapping("/claimSubmission")
    fun requestAgencyData(@RequestBody claimSubmission: SubmissionDTO): String {
        logger.info("submission received: $claimSubmission")
        val mapper = jacksonObjectMapper()
        runtimeService.startProcessInstanceByKey(
            "my-project-process",
            "myBizKey",
            mapOf("claimSubmission" to mapper.writeValueAsString(claimSubmission))
        )
        return "dont know how to return result..."
    }

    //region mock endpoint
    @GetMapping("/fsr/retrieveChild/{parentId}")
    fun requestFsrData(@PathVariable parentId: String): FsrResponseDTO {
        logger.info("request fsr child for parent: $parentId")
        val childId = if (parentId == "S1") "T1" else "T99"
        return FsrResponseDTO(
            parentId = parentId,
            spouseId = "ABC",
            childId = childId
        )
    }

    @PostMapping("/flowClaimCalculation")
    fun flowClaimCalculation(@RequestBody request: OdmSubmissionDTO): OdmMaxClaimDTO {
        logger.info("flowClaimCalculation: $request")
        val rawClaimAmount = request.mySubmissionDetails.numberOfDays * request.mySubmissionDetails.salaryPerDay
        val maxClaimAmount = if (rawClaimAmount > 1000) 1000 else rawClaimAmount
        return OdmMaxClaimDTO(
            OdmMaxClaimDetailsDTO(
                maxClaimAmount,
                request.mySubmissionDetails.parentId,
                request.mySubmissionDetails.childId
            )
        )
    }

    @PostMapping("/flowValidation")
    fun flowValidation(@RequestBody request: OdmValidationDTO): OdmClaimStatusDTO {
        logger.info("flowValidation: $request")
        val isChildMatch =
            request.myValidationDetails.childId == request.myValidationDetails.icaChildId &&
                request.myValidationDetails.childId == request.myValidationDetails.fsrChildId
        val claimAmount = request.myValidationDetails.maxClaimAmount
        return OdmClaimStatusDTO(
            myClaimStatus = if (isChildMatch) "Approve $claimAmount" else "Reject $claimAmount"
        )
    }
    //endregion
}

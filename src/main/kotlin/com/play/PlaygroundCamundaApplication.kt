package com.play

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.openfeign.EnableFeignClients

@SpringBootApplication
@EnableFeignClients
class PlaygroundCamundaApplication

fun main(args: Array<String>) {
    runApplication<PlaygroundCamundaApplication>(*args)
}

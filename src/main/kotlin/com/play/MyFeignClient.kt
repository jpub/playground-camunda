package com.play

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import feign.Client
import feign.Logger
import feign.codec.Decoder
import feign.codec.Encoder
import feign.jackson.JacksonDecoder
import feign.jackson.JacksonEncoder
import feign.okhttp.OkHttpClient
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.context.annotation.Bean
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import javax.annotation.PostConstruct

class MyFeignConfiguration() {
    private var okHttpClient: okhttp3.OkHttpClient? = null

    @PostConstruct
    fun init() {
        okHttpClient = okhttp3.OkHttpClient.Builder().build()
    }

    @Bean
    fun feignLoggerLevel() = Logger.Level.FULL

    @Bean
    fun feignDecoder(): Decoder = JacksonDecoder(jacksonObjectMapper())

    @Bean
    fun feignEncoder(): Encoder = JacksonEncoder(jacksonObjectMapper())

    @Bean
    fun client(): Client {
        return OkHttpClient(okHttpClient)
    }
}

@FeignClient(
    name = "OdmClient",
//    url = "http://localhost:9060/DecisionService/rest/deployClaimRule",
    url = "\${odm-client.url}",
    configuration = [MyFeignConfiguration::class]
)
interface OdmClient {
    @RequestMapping(
        method = [RequestMethod.POST],
        path = ["/flowClaimCalculation"],
        consumes = [MediaType.APPLICATION_JSON_VALUE]
    )
    fun retrieveMaxClaim(bodyContent: OdmSubmissionDTO): OdmMaxClaimDTO

    @RequestMapping(
        method = [RequestMethod.POST],
        path = ["/flowValidation"],
        consumes = [MediaType.APPLICATION_JSON_VALUE]
    )
    fun retrieveClaimValidation(bodyContent: OdmValidationDTO): OdmClaimStatusDTO
}

@FeignClient(
    name = "FsrClient",
    url = "http://localhost:7700/api",
    configuration = [MyFeignConfiguration::class]
)
interface FsrClient {
    @RequestMapping(method = [RequestMethod.GET], path = ["/fsr/retrieveChild/{parentId}"])
    fun requestFsrData(@PathVariable parentId: String): FsrResponseDTO
}

package com.play

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

data class SubmissionDTO(
    val parentId: String = "",
    val childId: String = "",
    val numberOfDays: Int = 0,
    val salaryPerDay: Int = 0,
)

data class OdmSubmissionDetailsDTO(
    val parentId: String = "",
    val childId: String = "",
    val numberOfDays: Int = 0,
    val salaryPerDay: Int = 0,
)

data class OdmSubmissionDTO(
    val mySubmissionDetails: OdmSubmissionDetailsDTO = OdmSubmissionDetailsDTO()
)

data class OdmMaxClaimDetailsDTO(
    val maxClaimAmount: Int = 0,
    val parentId: String = "",
    val childId: String = "",
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class OdmMaxClaimDTO(
    val myMaxClaimDetails: OdmMaxClaimDetailsDTO = OdmMaxClaimDetailsDTO()
)

data class OdmValidationDetailsDTO(
    val maxClaimAmount: Int = 0,
    val parentId: String = "",
    val childId: String = "",
    val icaChildId: String = "",
    val fsrChildId: String = "",
    val fsrSpouseId: String = "",
)

data class OdmValidationDTO(
    val myValidationDetails: OdmValidationDetailsDTO = OdmValidationDetailsDTO()
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class OdmClaimStatusDTO(
    val myClaimStatus: String = ""
)

data class IcaResponseDTO(
    var parentId: String = "",
    var childId: String = "",
)

data class FsrResponseDTO(
    var parentId: String = "",
    var spouseId: String = "",
    var childId: String = "",
)

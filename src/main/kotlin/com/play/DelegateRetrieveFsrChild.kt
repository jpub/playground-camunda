package com.play

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.apache.camel.ExchangePattern
import org.apache.camel.ProducerTemplate
import org.camunda.bpm.engine.delegate.DelegateExecution
import org.camunda.bpm.engine.delegate.JavaDelegate
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class DelegateRetrieveFsrChild : JavaDelegate {
    @Autowired
    private lateinit var fsrClient: FsrClient

    @Autowired
    private lateinit var producerTemplate: ProducerTemplate

    private val logger = LoggerFactory.getLogger(this.javaClass)

    override fun execute(execution: DelegateExecution) {
        logger.info("exeActivityName: ${execution.currentActivityName}")
        val mapper = jacksonObjectMapper()
        val parentId = execution.getVariable("DelegateRetrieveChildren") as String
        val fsrResponse = fsrClient.requestFsrData(parentId)
        execution.setVariable("DelegateRetrieveFsrChild", mapper.writeValueAsString(fsrResponse))

        // test with camel
        val response = producerTemplate.sendBody("direct:fsr-child-request", ExchangePattern.InOut, parentId)
        execution.setVariable("DelegateRetrieveFsrChild-camel", response)
    }
}

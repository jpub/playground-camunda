package com.play

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.apache.camel.ProducerTemplate
import org.apache.camel.ExchangePattern
import org.camunda.bpm.engine.delegate.DelegateExecution
import org.camunda.bpm.engine.delegate.JavaDelegate
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class DelegateClaimReceived : JavaDelegate {
    @Autowired
    private lateinit var odmClient: OdmClient

    @Autowired
    private lateinit var producerTemplate: ProducerTemplate

    private val logger = LoggerFactory.getLogger(this.javaClass)

    override fun execute(execution: DelegateExecution) {
        logger.info("exeActivityName: ${execution.currentActivityName}")
        val mapper = jacksonObjectMapper()
        val claimSubmission = execution.getVariable("claimSubmission") as String
        val submissionDTO = mapper.readValue(claimSubmission, SubmissionDTO::class.java)
        val odmSubmissionDTO = OdmSubmissionDTO(
            OdmSubmissionDetailsDTO(
                parentId = submissionDTO.parentId,
                childId = submissionDTO.childId,
                numberOfDays = submissionDTO.numberOfDays,
                salaryPerDay = submissionDTO.salaryPerDay
            )
        )
        val odmResponse = odmClient.retrieveMaxClaim(odmSubmissionDTO)
        execution.setVariable("DelegateClaimReceived", mapper.writeValueAsString(odmResponse))

        // test with camel
        val response = producerTemplate.sendBody("direct:odm-claim-submission", ExchangePattern.InOut, odmSubmissionDTO)
        execution.setVariable("DelegateClaimReceived-camel", response)
    }
}

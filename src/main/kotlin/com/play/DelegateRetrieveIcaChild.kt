package com.play

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.camunda.bpm.engine.delegate.DelegateExecution
import org.camunda.bpm.engine.delegate.JavaDelegate
import org.slf4j.LoggerFactory

class DelegateRetrieveIcaChild : JavaDelegate {
    private val logger = LoggerFactory.getLogger(this.javaClass)

    override fun execute(execution: DelegateExecution) {
        logger.info("exeActivityName: ${execution.currentActivityName}")
        val mapper = jacksonObjectMapper()
        val parentId = execution.getVariable("DelegateRetrieveChildren") as String
        val icaResponseDTO = IcaResponseDTO(
            parentId = parentId,
            childId = if (parentId == "S1") "T1" else "T88"
        )
        execution.setVariable("DelegateRetrieveIcaChild", mapper.writeValueAsString(icaResponseDTO))
    }
}

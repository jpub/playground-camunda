package com.play

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.camunda.bpm.engine.delegate.DelegateExecution
import org.camunda.bpm.engine.delegate.JavaDelegate
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class DelegateValidateClaim : JavaDelegate {
    @Autowired
    private lateinit var odmClient: OdmClient

    private val logger = LoggerFactory.getLogger(this.javaClass)

    override fun execute(execution: DelegateExecution) {
//        logger.info("exeActivityName: ${execution.currentActivityName}")
//        val abc = execution.getVariable("DelegateRetrieveIcaChild") as String
//        println(abc)
// //        val xxx = execution.getVariable("DelegateRetrieveFsrChild") as String
// //        println(xxx)
//        execution.setVariable("DelegateValidateClaim", "okloh")

        logger.info("exeActivityName: ${execution.currentActivityName}")
        val mapper = jacksonObjectMapper()

        val claimSubmission = execution.getVariable("claimSubmission") as String
        val maxClaimResponse = execution.getVariable("DelegateClaimReceived") as String
        val fsrResponse = execution.getVariable("DelegateRetrieveFsrChild") as String
        val icaResponse = execution.getVariable("DelegateRetrieveIcaChild") as String

        val submissionDTO = mapper.readValue(claimSubmission, SubmissionDTO::class.java)
        val odmMaxClaimDTO = mapper.readValue(maxClaimResponse, OdmMaxClaimDTO::class.java)
        val fsrResponseDTO = mapper.readValue(fsrResponse, FsrResponseDTO::class.java)
        val icaResponseDTO = mapper.readValue(icaResponse, IcaResponseDTO::class.java)

        val odmValidationDTO = OdmValidationDTO(
            OdmValidationDetailsDTO(
                maxClaimAmount = odmMaxClaimDTO.myMaxClaimDetails.maxClaimAmount,
                parentId = submissionDTO.parentId,
                childId = submissionDTO.childId,
                icaChildId = icaResponseDTO.childId,
                fsrChildId = fsrResponseDTO.childId,
                fsrSpouseId = fsrResponseDTO.spouseId
            )
        )
        val odmResponse = odmClient.retrieveClaimValidation(odmValidationDTO)
        execution.setVariable("DelegateValidateClaim", mapper.writeValueAsString(odmResponse))
    }
}
